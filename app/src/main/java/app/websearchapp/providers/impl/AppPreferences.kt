package app.websearchapp.providers.impl

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import app.websearchapp.di.qualifiers.ApplicationQualifier
import app.websearchapp.main.SearchConfig
import app.websearchapp.providers.Preferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPreferences : Preferences {

    private var preferences: SharedPreferences? = null

    @Inject
    constructor(@ApplicationQualifier context: Context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    companion object {
        private const val URL = "pref_key_url"
        private const val COUNT_URL = "pref_key_count_url"
        private const val COUNT_THREAD = "pref_key_count_thread"
        private const val TEXT_SEARCH = "pref_key_text_search"
    }



    /*@Inject
    internal fun AppPreferences(@ApplicationQualifier context: Context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }*/

    override fun setInitialUrl(url: String) {
        preferences!!.edit().putString(URL, url).apply()
    }

    override fun getInitialUrl(): String {
        return preferences!!.getString(URL, SearchConfig.URL_DEFAULT)!!
    }

    override fun setMaxUrlCount(count: Int) {
        preferences!!.edit().putInt(COUNT_URL, count).apply()
    }

    override fun getMaxUrlCount(): Int {
        return preferences!!.getInt(COUNT_URL, SearchConfig.COUNT_URL_DEFAULT)
    }

    override fun setMaxThreadCount(count: Int) {
        preferences!!.edit().putInt(COUNT_THREAD, count).apply()
    }

    override fun getMaxThreadCount(): Int {
        return preferences!!.getInt(COUNT_THREAD, SearchConfig.COUNT_THREAD_DEFAULT)
    }

    override fun setSearchText(text: String) {
        preferences!!.edit().putString(TEXT_SEARCH, text).apply()
    }

    override fun getSearchText(): String {
        return preferences!!.getString(TEXT_SEARCH, SearchConfig.TEXT_SEARCH_DEFAULT)!!
    }

    override fun setAllSettings(url: String, searchText: String, maxUrls: Int, maxThreads: Int) {
        preferences!!.edit()
            .putString(URL, url)
            .putString(TEXT_SEARCH, searchText)
            .putInt(COUNT_URL, maxUrls)
            .putInt(COUNT_THREAD, maxThreads)
            .apply()
    }
}