package app.websearchapp.providers.impl

import android.content.Context
import android.os.Build
import android.widget.Toast
import androidx.annotation.StringRes
import app.websearchapp.di.qualifiers.ApplicationQualifier
import app.websearchapp.providers.Reporter
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppReporter : Reporter {

    private var context: Context? = null

    @Inject
    constructor(@ApplicationQualifier context: Context?) {
        this.context = context
    }

    override fun report(@StringRes messageRes: Int) {
        report(context!!.getString(messageRes))
    }

    override fun report(message: String, vararg args: Any) {
        val formatted = String.format(getCurrentLocale(), message, *args)
        report(formatted)
    }

    override fun report(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    @Suppress("DEPRECATION")
    private fun getCurrentLocale(): Locale {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context!!.resources.configuration.locales.get(0)
        } else {
            context!!.resources.configuration.locale
        }
    }
}