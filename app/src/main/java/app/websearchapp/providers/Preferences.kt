package app.websearchapp.providers

interface Preferences {

    fun setInitialUrl(url: String)

    fun getInitialUrl(): String

    fun setMaxUrlCount(count: Int)

    fun getMaxUrlCount(): Int

    fun setMaxThreadCount(count: Int)

    fun getMaxThreadCount(): Int

    fun setSearchText(text: String)

    fun getSearchText(): String

    fun setAllSettings(url: String, searchText: String, maxUrls: Int, maxThreads: Int)

}