package app.websearchapp.providers

import androidx.annotation.StringRes

interface Reporter {

    fun report(@StringRes messageRes: Int)

    fun report(message: String, vararg args: Any)

    fun report(message: String)

}