package app.websearchapp.di

import android.content.Context
import app.websearchapp.App
import app.websearchapp.di.qualifiers.ApplicationQualifier
import app.websearchapp.providers.impl.AppPreferences
import app.websearchapp.providers.impl.AppReporter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    @ApplicationQualifier
    fun context(): Context

    fun preferences(): AppPreferences

    fun reporter(): AppReporter

    fun inject(app: App)

}