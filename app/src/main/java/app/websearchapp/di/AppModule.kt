package app.websearchapp.di

import android.content.Context
import app.websearchapp.di.qualifiers.ApplicationQualifier
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    @ApplicationQualifier
    internal fun provideContext(): Context {
        return context
    }

}