package app.websearchapp.util

import android.util.Patterns
import java.util.regex.Pattern

class CommonUtils {

    companion object {

        private val urlPattern = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")

        fun getUrlPattern(): Pattern {
            return Patterns.WEB_URL
        }

        fun isUrlValid(url: String): Boolean {
            return url.startsWith("http") && urlPattern.matcher(url).matches()
        }

        fun isAnyNullOrEmpty(vararg strings: String?): Boolean {
            for (s in strings)
                if (s.isNullOrEmpty()) return true
            return false
        }
    }
}