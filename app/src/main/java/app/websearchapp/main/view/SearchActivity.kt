package app.websearchapp.main.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import app.websearchapp.R
import app.websearchapp.main.model.SearchInteractor
import app.websearchapp.main.model.Url
import app.websearchapp.main.presenter.SearchPresenter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.widget_progress_view.*


class SearchActivity : AppCompatActivity(R.layout.activity_main), SearchView {

    private lateinit var mainPresenter: SearchPresenter

    private var progressView: View? = null
    private var urlsAdapter: UrlItemsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainPresenter = SearchPresenter(this, SearchInteractor())
        mainPresenter.onCreate()
        initUrlsRv()
        generateProgressView()

        tvSettings.setOnClickListener { foldUnfoldSettings() }
        fabStartStop.setOnClickListener { onStartStopClick() }
        btnPause.setOnClickListener { mainPresenter.onPauseClick() }
        btnPause.isSelected = true
    }

    override fun onDestroy() {
        mainPresenter.onDestroy()
        super.onDestroy()
    }

    private fun foldUnfoldSettings() {
        llSettings.visibility =
            if (llSettings.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    private fun initUrlsRv() {
        urlsAdapter = UrlItemsAdapter()
        rvUrls.setHasFixedSize(true)
        rvUrls.itemAnimator = null
        rvUrls.addItemDecoration(
            DividerItemDecoration(
                rvUrls.context,
                DividerItemDecoration.VERTICAL
            )
        )
        rvUrls.layoutManager = LinearLayoutManager(this)
        rvUrls.adapter = urlsAdapter
    }

    override fun setUrlsList(urls: List<Url>) {
        runOnUiThread {
            urlsAdapter?.setUrlsList(urls)
        }
    }

    override fun onUrlItemChanged(pos: Int) {
        runOnUiThread {
            urlsAdapter?.onUrlItemChanged(pos)
        }
    }

    override fun onUrlItemInserted(pos: Int) {
        runOnUiThread {
            urlsAdapter?.onUrlItemInserted(pos)
        }
    }

    override fun setSettings(url: String, searchText: String, maxUrls: Int, maxThreads: Int) {
        etUrlValue.setText(url)
        etSearchText.setText(searchText)
        etUrlCount.setText(maxUrls.toString())
        etThreadCount.setText(maxThreads.toString())
    }

    private fun onStartStopClick() {
        mainPresenter.onStartStopClick(
            etUrlValue.text.toString(),
            etSearchText.text.toString(),
            etUrlCount.text.toString(),
            etThreadCount.text.toString()
        )
    }

    override fun setStartStopBtnState(stopIcon: Boolean) {
        runOnUiThread {
            fabStartStop.isSelected = stopIcon
            btnPause.visibility = if (stopIcon) View.VISIBLE else View.GONE
        }
    }

    override fun setPauseContinueBtnState(continueIcon: Boolean) {
        btnPause.isSelected = !continueIcon
    }

    override fun hideRedundantViews() {
        llSettings?.visibility = View.GONE
        hideSoftKeyboard()
    }

    override fun showProgress(mVal: Int, maxVal: Int) {
        runOnUiThread {
            progressView?.visibility = View.VISIBLE
            pBar.max = maxVal
            pBar.setProgress(mVal, true)
        }
    }

    override fun hideProgress() {
        runOnUiThread {
            progressView?.visibility = View.GONE
        }
    }

    private fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
        }
    }

    @SuppressLint("InflateParams")
    private fun generateProgressView() {
        progressView = window.layoutInflater.inflate(R.layout.widget_progress_view, null, false)
        addContentView(
            progressView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
    }
}
