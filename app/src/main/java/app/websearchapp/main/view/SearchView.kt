package app.websearchapp.main.view

import app.websearchapp.main.model.Url

interface SearchView {

    fun showProgress(mVal: Int, maxVal: Int)
    fun hideProgress()
    fun setSettings(url: String, searchText: String, maxUrls: Int, maxThreads: Int)
    fun setStartStopBtnState(stopIcon: Boolean)
    fun setPauseContinueBtnState(continueIcon: Boolean)
    fun setUrlsList(urls: List<Url>)
    fun onUrlItemChanged(pos: Int)
    fun onUrlItemInserted(pos: Int)
    fun hideRedundantViews()
}