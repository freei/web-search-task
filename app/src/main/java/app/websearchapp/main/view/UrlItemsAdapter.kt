package app.websearchapp.main.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.websearchapp.R
import app.websearchapp.main.model.Url
import kotlinx.android.synthetic.main.item_url.view.*

class UrlItemsAdapter : RecyclerView.Adapter<UrlItemsAdapter.UrlViewHolder>() {

    private var urls: List<Url>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UrlViewHolder {
        val item = LayoutInflater.from(parent.context).inflate(R.layout.item_url, parent, false)
        return UrlViewHolder(item)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: UrlViewHolder, position: Int) {
        if (position == RecyclerView.NO_POSITION) return

        val url = urls!![position]

        val statusOrdinal = Url.Status.ordinalOf(url.status!!.name)
        val statusColor = Url.Status.getColorByTypeId(statusOrdinal)

        holder.tvUrl.text = url.url

        holder.tvSearchStatus.text =
            if (statusOrdinal == Url.Status.ERROR.ordinal)
                url.errorMessage
            else
                url.status!!.name


//        holder.tvUrl.setTextColor(statusColor)

        holder.tvSearchStatus.setBackgroundColor(statusColor)
    }

    override fun getItemCount(): Int {
        return if (urls == null) 0 else urls!!.size
    }

    fun setUrlsList(urls: List<Url>) {
        this.urls = urls
        notifyDataSetChanged()
    }

    fun onUrlItemChanged(pos: Int) {
        notifyItemChanged(pos)
    }

    fun onUrlItemInserted(pos: Int) {
//        notifyDataSetChanged()
        notifyItemInserted(pos)
    }

    inner class UrlViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvUrl: TextView = itemView.tvUrl
        val tvSearchStatus: TextView = itemView.tvSearchStatus
    }
}