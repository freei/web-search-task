package app.websearchapp.main.presenter

import app.websearchapp.App
import app.websearchapp.R
import app.websearchapp.main.SearchConfig
import app.websearchapp.main.model.InteractorCallback
import app.websearchapp.main.model.SearchInteractor
import app.websearchapp.main.model.Url
import app.websearchapp.main.view.SearchView
import app.websearchapp.util.CommonUtils
import java.util.*

class SearchPresenter(private var view: SearchView?, private var interactor: SearchInteractor) :
    InteractorCallback {

    fun onCreate() {
        interactor.requestSettings(this)
    }

    fun onDestroy() {
        view = null
    }


    fun onPauseClick() {
        interactor.requestLoadingPauseContinue()
    }

    fun onStartStopClick(url: String, searchText: String, maxUrls: String, maxThreads: String) {
        if (!isUserDataValid(url, searchText, maxUrls, maxThreads)) {
            return
        }
        toggleTaskRunByUser(url, searchText, maxUrls, maxThreads)
    }

    private fun toggleTaskRunByUser(
        url: String,
        searchText: String,
        maxUrls: String,
        maxThreads: String
    ) {
        if (interactor.getIsLoading()) {
            interactor.requestLoadingStop()
        } else {
            view?.hideRedundantViews()
            interactor.requestLoadingStart(
                url,
                searchText,
                maxUrls.toInt(),
                maxThreads.toInt(),
                this
            )
        }
    }


    override fun onUrlsListInit(urls: List<Url>) {
        view?.setStartStopBtnState(true)
        view?.setUrlsList(urls)
    }

    override fun onLoadingStop() {
        view?.hideProgress()
        view?.setStartStopBtnState(false)
    }

    override fun onLoadingPauseContinue(pause: Boolean) {
        view?.setPauseContinueBtnState(pause)
    }

    override fun onProgressUpdate(mVal: Int, maxVal: Int) {
        view?.showProgress(mVal, maxVal)
    }

    override fun onUrlItemChanged(pos: Int) {
        view?.onUrlItemChanged(pos)
    }

    override fun onUrlItemInserted(pos: Int) {
        view?.onUrlItemInserted(pos)
    }

    private fun isUserDataValid(
        url: String,
        searchText: String,
        maxUrls: String,
        maxThreads: String
    ): Boolean {
        if (CommonUtils.isAnyNullOrEmpty(url, searchText, maxUrls, maxThreads)) {
            App.getAppComponent().reporter()
                .report(App.getAppComponent().context().getString(R.string.fields_must_not_be_empty))
            return false
        }
        if (!CommonUtils.isUrlValid(url)) {
            App.getAppComponent().reporter()
                .report(App.getAppComponent().context().getString(R.string.invalid_url))
            return false
        }
        if (maxUrls.toInt() < SearchConfig.COUNT_URL_MIN) {
            App.getAppComponent().reporter().report(
                String.format(
                    Locale.getDefault(),
                    App.getAppComponent().context().getString(R.string.min_url_count_is),
                    SearchConfig.COUNT_URL_MIN
                )
            )
            return false
        }
        if (maxThreads.toInt() < SearchConfig.COUNT_THREAD_MIN) {
            App.getAppComponent().reporter().report(
                String.format(
                    Locale.getDefault(),
                    App.getAppComponent().context().getString(R.string.min_thread_count_is),
                    SearchConfig.COUNT_THREAD_MIN
                )
            )
            return false
        }
        return true
    }

    override fun onSettingsReceived(
        url: String,
        searchText: String,
        maxUrls: Int,
        maxThreads: Int
    ) {
        view?.setSettings(url, searchText, maxUrls, maxThreads)
    }
}