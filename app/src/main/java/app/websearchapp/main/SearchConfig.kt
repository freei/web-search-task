package app.websearchapp.main

object SearchConfig {
    internal const val URL_DEFAULT: String = "https://electroavtosam.com.ua"
    internal const val TEXT_SEARCH_DEFAULT: String = "leaf"
    internal const val COUNT_URL_DEFAULT = 999
    internal const val COUNT_THREAD_DEFAULT = 30
    internal const val COUNT_URL_MIN = 1
    internal const val COUNT_THREAD_MIN = 1

    /*connection pool config*/
    internal const val COUNT_IDLE_CONNECTIONS_MAX = 100
    internal const val DURATION_KEEP_ALIVE = 30L     /*secs*/
}