package app.websearchapp.main.model

import android.util.Log
import app.websearchapp.App
import app.websearchapp.main.SearchConfig
import app.websearchapp.util.CommonUtils
import okhttp3.*
import java.io.IOException
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.TimeUnit


class SearchInteractor {
    companion object {
        private val TAG: String = SearchInteractor::class.java.simpleName
    }

    private val urlsQueue: ConcurrentLinkedQueue<Url> = ConcurrentLinkedQueue()
    private val urlsHandled: MutableSet<Url> = ConcurrentHashMap.newKeySet<Url>()
    private var urlsDisplayList: MutableList<Url>? = null
    private var searchText: String? = null
    private var maxUrls: Int = SearchConfig.COUNT_URL_DEFAULT
    private var dataCallback: InteractorCallback? = null

    private var okHttpClient: OkHttpClient? = null
    private var pausedRequests: List<Call>? = null
    private var state = State.IDLE

    enum class State {
        IDLE, LOADING, PAUSED, STOPPED, FINISHED;
    }

    fun getIsLoading(): Boolean {
        return state == State.LOADING || state == State.PAUSED
    }

    private fun onUrlsLoadingFinished() {
        state = State.FINISHED
        dataCallback?.onLoadingStop()
    }

    fun requestLoadingStop() {
        state = State.STOPPED
        onUrlsLoadingFinished()
        okHttpClient?.dispatcher?.cancelAll()
    }

    fun requestLoadingPauseContinue() {
        dataCallback?.onLoadingPauseContinue(state == State.LOADING)    /*negated intentionally*/
        if (state == State.LOADING) {
            state = State.PAUSED
            val dispatcher = okHttpClient?.dispatcher ?: return
            pausedRequests =                  /*save queued requests*/
                dispatcher.runningCalls()
                    .plus(dispatcher.queuedCalls())
            dispatcher.cancelAll()
            return
        }
        else if (state == State.PAUSED) {
            state = State.LOADING
            if (pausedRequests != null) {    /*continue loading*/
                for (call in pausedRequests!!) {
                    downloadPage(call.request().tag() as Url)
                }
                startWebScan()
            }
        }
    }

    fun requestLoadingStart(
        url: String,
        searchText: String,
        maxUrls: Int,
        maxThreads: Int,
        dataCallback: InteractorCallback
    ) {
        this.searchText = searchText
        this.maxUrls = maxUrls
        this.dataCallback = dataCallback
        saveSettings(url, searchText, maxUrls, maxThreads)

        setupHttpClient(maxThreads)
        setInitialData(url)
        state = State.LOADING
        startWebScan()
    }

    private fun setupHttpClient(maxThreads: Int) {
        okHttpClient = OkHttpClient.Builder()
            .connectionPool(
                ConnectionPool(
                    SearchConfig.COUNT_IDLE_CONNECTIONS_MAX,
                    SearchConfig.DURATION_KEEP_ALIVE,
                    TimeUnit.SECONDS
                )
            )
            .build()
        okHttpClient?.dispatcher?.maxRequests = maxThreads
    }

    private fun setInitialData(url: String) {
        if (urlsHandled.isNotEmpty()) urlsHandled.clear()
        if (urlsQueue.isNotEmpty()) urlsQueue.clear()
        urlsDisplayList = Collections.synchronizedList<Url>(ArrayList())
        dataCallback?.onUrlsListInit(urlsDisplayList!!)
        val urlModel = Url(url)
        urlsQueue.add(urlModel)
    }

    private fun startWebScan() {
        while (state == State.LOADING && urlsQueue.isNotEmpty()) {
            //search among the maxUrls quantity
            if (urlsHandled.size > maxUrls - 1) {
                // at this point only old URLs remain unprocessed, the new ones will not be added/taken from the Queue
                return
            }

            val url = urlsQueue.poll() ?: continue

            addUrlToHandledAndDisplayList(url)
            downloadPage(url)
        }
    }

    private fun downloadPage(url: Url) {
        url.status = Url.Status.LOADING
        dataCallback?.onUrlItemChanged(url.index!!)

        val request = Request.Builder()
            .url(url.url)
            .tag(url)
            .build()

        okHttpClient?.newCall(request)?.enqueue(getResponseCallback(url))
    }


    private fun getResponseCallback(url: Url): Callback {
        return object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                if (state == State.PAUSED) {
                    setIdleAndUpdateUi(url)
                    return
                }
                calcProgressOnResponse()
                setErrorAndUpdateUi(url, "resp: ".plus(e.localizedMessage))
                Log.d(TAG, "downloadPage: newCall onFailure")
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                calcProgressOnResponse()
                val inputStream = response.body?.byteStream()
                if (inputStream == null) {
                    setErrorAndUpdateUi(url, "empty\nresponse")
                    return
                }

                url.contentText = try {
                    inputStream
                        .bufferedReader()
                        .use { it.readText() }
                } catch (e: IOException) {
                    setErrorAndUpdateUi(url, e.localizedMessage)
                    e.localizedMessage
                }

                if (url.contentText.isNullOrEmpty()) {
                    setErrorAndUpdateUi(url, "empty\ncontent")
                    return
                }
                searchTextOnPage(searchText!!, url)
                searchUrlsOnPage(url.contentText!!)
            }
        }
    }

    private fun calcProgressOnResponse() {
        if (state == State.PAUSED) return
        val callsInQueue =              /*decreasing from maxUrls to 0*/
            okHttpClient?.dispatcher?.queuedCallsCount() ?: 0

        val mUrls = urlsHandled.size
        if (mUrls > 1)
            dataCallback?.onProgressUpdate(mUrls - callsInQueue, mUrls)
        if (urlsQueue.isNotEmpty() && mUrls > maxUrls - 1) {
            if (callsInQueue == 0)
                onUrlsLoadingFinished()
        }
    }

    private fun setIdleAndUpdateUi(url: Url) {
        url.status = Url.Status.IDLE
        dataCallback?.onUrlItemChanged(url.index!!)
    }

    private fun setErrorAndUpdateUi(url: Url, errorMsg: String) {
        url.status = Url.Status.ERROR
        url.errorMessage = errorMsg
        dataCallback?.onUrlItemChanged(url.index!!)
    }

    private fun searchTextOnPage(text: String, url: Url) {
        url.status =
            if (url.contentText.isNullOrEmpty() || !url.contentText!!.contains(text)) Url.Status.NOT_FOUND
            else Url.Status.FOUND
        dataCallback?.onUrlItemChanged(url.index!!)
    }

    private fun searchUrlsOnPage(contentText: String) {
        val matcher = CommonUtils.getUrlPattern().matcher(contentText)

        while (matcher.find()) {
            val newUrl = matcher.group()
            if (newUrl.isEmpty() || !newUrl.startsWith("http")) continue
            optEnqueueNewUrl(newUrl)
        }
    }

    private fun optEnqueueNewUrl(newUrl: String) {
        optEnqueueNewUrl(newUrl, Url.Status.IDLE)
    }

    private fun optEnqueueNewUrl(newUrl: String, status: Url.Status) {
        if (urlsQueue.size < maxUrls && !containsUrl(urlsHandled, newUrl)) {
            val urlModel = Url(newUrl)
            if (!urlsQueue.offer(urlModel)) {
                Log.d(TAG, "optEnqueueNewUrl: FAILED to offer a new Url item to the urlsQueue")
                addUrlToHandledAndDisplayList(urlModel)
                setErrorAndUpdateUi(urlModel, "enqueue\nfailed")
            } else {
                startWebScan()
            }
        }
    }

    @Synchronized
    private fun addUrlToHandledAndDisplayList(url: Url) {
        urlsHandled.add(url)
        urlsDisplayList!!.add(url)
        url.index = urlsDisplayList!!.lastIndex
        dataCallback?.onUrlItemInserted(url.index!!)
    }

    private fun containsUrl(handled: Set<Url>, url: String): Boolean {
        return handled.stream().anyMatch { o -> o.url == url }
    }

    fun requestSettings(dataCallback: InteractorCallback) {
        val url = App.getAppComponent().preferences().getInitialUrl()
        val searchText = App.getAppComponent().preferences().getSearchText()
        val maxUrls = App.getAppComponent().preferences().getMaxUrlCount()
        val maxThreads = App.getAppComponent().preferences().getMaxThreadCount()
        dataCallback.onSettingsReceived(url, searchText, maxUrls, maxThreads)
    }

    private fun saveSettings(url: String, searchText: String, maxUrls: Int, maxThreads: Int) {
        App.getAppComponent().preferences().setAllSettings(url, searchText, maxUrls, maxThreads)
    }
}
