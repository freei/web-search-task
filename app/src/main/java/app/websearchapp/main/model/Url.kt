package app.websearchapp.main.model

import androidx.core.content.ContextCompat
import app.websearchapp.App
import java.util.*

data class Url(val url: String) {

    var index: Int? = null
    var contentText: String? = null
    var status: Status? = Status.IDLE
    var errorMessage: String? = null

    enum class Status(val value: Int) {
        IDLE(0), LOADING(1), FOUND(2), NOT_FOUND(3), ERROR(4);

        companion object {
            private val idleColor = ContextCompat.getColor(App.getAppComponent().context(), android.R.color.tab_indicator_text)
            private val loadingColor = ContextCompat.getColor(App.getAppComponent().context(), android.R.color.holo_blue_dark)
            private val foundColor = ContextCompat.getColor(App.getAppComponent().context(), android.R.color.holo_green_light)
            private val notFoundColor = ContextCompat.getColor(App.getAppComponent().context(), android.R.color.background_dark)
            private val errorColor = ContextCompat.getColor(App.getAppComponent().context(), android.R.color.holo_red_light)

            private val map = HashMap<Int, Status>()

            init {
                for (s in values()) {
                    map[s.value] = s
                }
            }

            fun valueOf(ordinal: Int): Status? {
                return map[ordinal]
            }

            fun ordinalOf(status: String) : Int {
                for (enum in values()) {
                    if (enum.toString().equals(status, true))
                        return enum.ordinal
                }
                throw IllegalArgumentException()
            }

            fun getColorByTypeId(ordinal: Int) : Int {
                return when (ordinal) {
                    0 -> idleColor
                    1 -> loadingColor
                    2 -> foundColor
                    3 -> notFoundColor
                    4 -> errorColor
                    else -> idleColor
                }
            }
        }
    }
}