package app.websearchapp.main.model

interface InteractorCallback {
        fun onUrlsListInit(urls: List<Url>)
        fun onLoadingStop()
        fun onLoadingPauseContinue(pause: Boolean)
        fun onUrlItemChanged(pos: Int)
        fun onUrlItemInserted(pos: Int)

        fun onProgressUpdate(mVal: Int, maxVal: Int)
        fun onSettingsReceived(url: String, searchText: String, maxUrls: Int, maxThreads: Int)
}