package app.websearchapp

import android.app.Application
import app.websearchapp.di.AppComponent
import app.websearchapp.di.AppModule
import app.websearchapp.di.DaggerAppComponent

class App : Application() {

    private var component: AppComponent? = null

    init {
        instance = this
    }

    companion object {
        @Volatile
        private var instance: App? = null

        fun getAppComponent() : AppComponent {
            if (instance!!.component == null) {
                instance!!.buildAppComponent()
            }
            return instance!!.component!!
        }
    }

    override fun onCreate() {
        super.onCreate()
        buildAppComponent()
    }

    private fun buildAppComponent() {
        component = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        component!!.inject(this)
    }

}